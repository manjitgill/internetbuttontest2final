//
//  ViewController.swift
//  InternetButtonTest2Final
//
//  Created by yaad on 2019-11-07.
//  Copyright © 2019 yaad. All rights reserved.
//

import UIKit
import Particle_SDK


class ViewController: UIViewController {
    
    let USERNAME = "msinghgill1993@yahoo.com"
       let PASSWORD = "M1o2n3u4@"
    var seconds = 0
    var sVal : Int = 1
    var timer = Timer()
     @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var slowTimeLabel: UILabel!
    @IBOutlet weak var amountOfMohammad: UISlider!
    
       // MARK: Device
       let DEVICE_ID = "2e0045001047363333343437"
       var myPhoton : ParticleDevice?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        // 1. Initialize the SDK

                      ParticleCloud.init()
                      // 2. Login to your account
                      ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
                          if (error != nil) {
                              // Something went wrong!
                              print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                              // Print out more detailed information
                              print(error?.localizedDescription)
                          }
                          else {
                              print("Login success!")
                              // try to get the device
                              self.getDeviceFromCloud()
                          }
                      } // end login
               
    }
    
    @IBAction func startTimerButton(_ sender: Any) {
        runTimer()
    }
     @objc
                 func updateTimer() {
                      sVal = Int(amountOfMohammad.value)
                    slowTimeLabel.text = "Time slows down by: \(sVal)"
                    if sVal == 5 {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 15.0) {
                            self.seconds += 1
                            self.secondCall(sec: self.seconds)
                        }
                    }
                    if sVal == 2 {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            self.seconds += 1
                            self.secondCall(sec: self.seconds)
                        }
                    }
                    if sVal == 3 {
                         DispatchQueue.main.asyncAfter(deadline: .now() + 6.0) {
                                                   self.seconds += 1
                            self.secondCall(sec: self.seconds)
                                               }
                    }
                    if sVal == 4 {
                         DispatchQueue.main.asyncAfter(deadline: .now() + 12.0) {
                                                   self.seconds += 1
                            self.secondCall(sec: self.seconds)
                                               }
                    }
                    if sVal == 1 {
                         DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                                    self.seconds += 1
                            self.secondCall(sec: self.seconds)
                                                }
                    }
                   
                    if seconds >= 20 {
                        seconds = 0
                    }
                     showtime()
                    
                 }
                func showtime()
                {
                    timeLabel.text = "\(seconds)"
                     
                                         
                                  }
                         
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
             }
            
    
     // MARK: Get Device from Cloud
            // Gets the device from the Particle Cloud
            // and sets the global device variable
            func getDeviceFromCloud() {
            ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
                    
                    if (error != nil) {
                        print("Could not get device")
                        print(error?.localizedDescription)
                        return
                    }
                    else {
                        print("Got photon: \(device?.id)")
                        self.myPhoton = device
                        // subscribe to events
                        self.subscribeToParticleEvents()
                    }
                    
                } // end getDevice()
            }
            
            
            

            
            func subscribeToParticleEvents() {
                var handler : Any?

                      handler = ParticleCloud.sharedInstance().subscribeToDeviceEvents(withPrefix: "playerChoice", deviceID: self.DEVICE_ID , handler: {

                              (event :ParticleEvent?, error : Error?) in

                          

                          if let _ = error {

                              print("could not subscribe to events")

                          } else {

                              print("got event with data \(event?.data)")

                              let choice = (event?.data)!

                              if (choice == "A") {
                                print("pressed aaaaaa")

                               // self.temperature(temp: "\(self.temperature)")
    //                              self.gameScore = self.gameScore + 1;

                              }

                              else if (choice == "B") {

                              //  self.percepitation(precep: "\(self.preception)")

                              }

                          }

                      })


            }
       
        func secondCall(sec : Int) {
            
            print("Second function call")
            
            let parameters = ["\(seconds)"]
            var task = myPhoton!.callFunction("sec", withArguments: parameters) {
                (resultCode : NSNumber?, error : Error?) -> Void in
                if (error == nil) {
                    print("Sent message to Particle to show second")
                }
                else {
                    print(error)
                }
            }
            //var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
            
        }
        
        
       


}

